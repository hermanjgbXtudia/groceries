﻿using System;
using Newtonsoft.Json;
using System.ComponentModel;
using Humanizer;

namespace GroceriesListApp.Domain
{
    
    public class Grocery
    {
        [JsonProperty(PropertyName = "id")]
        public string id  { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string  Name       { get; set; }

        [JsonProperty(PropertyName = "Amount")]
        public double Amount     { get; set; }

        [JsonProperty(PropertyName = "CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [JsonProperty(PropertyName = "version")]
        public DateTime version { get; set; }

        [JsonProperty(PropertyName = "createdAt")]
        public DateTime createdAt { get; set; }

        [JsonProperty(PropertyName = "updatedAt")]
        public DateTime updatedAt { get; set; }

        [JsonProperty(PropertyName = "deleted")]
        public bool deleted { get; set; }
        
        [Microsoft.WindowsAzure.MobileServices.Version]
        public string AzureVersion { get; set; }

        [JsonIgnore]
        public bool Marc { get; set; }

    }
}
