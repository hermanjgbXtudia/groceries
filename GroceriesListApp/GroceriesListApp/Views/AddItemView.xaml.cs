﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GroceriesListApp.Views
{
    public partial class AddItemView : ContentPage
    {

        ViewModels.AddItemViewModel Context;
        public AddItemView()
        {
            InitializeComponent();
            BindingContext = Context = new ViewModels.AddItemViewModel();

        }

        public void btnAdd(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
