﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GroceriesListApp.Views
{
    public partial class GroceryListView : ContentPage
    {
        ViewModels.GroceriesListViewModel Context;
        public GroceryListView()
        {
            InitializeComponent();
            BindingContext = Context = new ViewModels.GroceriesListViewModel();
            
        }

        public void btnAdd(object sender, EventArgs e) {
            Navigation.PushAsync(new AddItemView());
        }

        public void ActionSelected(object sender, EventArgs e) {
            GroceriesView.SelectedItem = null;
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            //GroceriesView.ItemsSource = null;
            //Context.groceryList.Clear();
            Context.LoadGroceriesCommand.Execute(null);
            //GroceriesView.ItemsSource = Context.groceryList;
        }
    }
}
