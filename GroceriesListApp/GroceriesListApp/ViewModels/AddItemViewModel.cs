﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace GroceriesListApp.ViewModels
{
    public class AddItemViewModel
    {
        public string Name { get; set; }
        public double Amount { get; set; } = 1;

        Helpers.AzureDataService AzureContext;
        public AddItemViewModel() {
            AzureContext = new Helpers.AzureDataService();
        }

        ICommand addGroceryCommand;
        public ICommand AddGroceryCommand =>
            addGroceryCommand ?? (addGroceryCommand = new Command(async () => await ExecuteAddGroceryCommandAsync()));

        async Task ExecuteAddGroceryCommandAsync()
        {
            try
            {
                Domain.Grocery temp = new Domain.Grocery{ Name = this.Name, Amount = this.Amount, CreatedOn = DateTime.Today };
                var grocery = await AzureContext.AddGrocery(temp);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("OH NO!" + ex);
                
            }
        }

    }
}
