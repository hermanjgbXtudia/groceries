﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;
using System.Windows.Input;
using Microsoft.WindowsAzure.MobileServices;
using System.Diagnostics;

namespace GroceriesListApp.ViewModels
{
    public class GroceriesListViewModel:INotifyPropertyChanged
    {
        Helpers.AzureDataService AzureContext;
        private ObservableCollection<Domain.Grocery> _groceryList = new ObservableCollection<Domain.Grocery>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public ObservableCollection<Domain.Grocery> groceryList
        {
            get { return _groceryList; }
            set { _groceryList = value; OnPropertyChanged("groceryList"); }
        }


        public GroceriesListViewModel()
        {
            AzureContext = new Helpers.AzureDataService();
        }

        ICommand loadGroceriesCommand;


        public ICommand LoadGroceriesCommand =>
            loadGroceriesCommand ?? (loadGroceriesCommand = new Command(async () => await ExecuteLoadGroceriesCommandAsync()));

        async Task ExecuteLoadGroceriesCommandAsync()
        {
            try
            {
                await AzureContext.Initialize();
                groceryList = new ObservableCollection<Domain.Grocery>(await AzureContext.GetGrocery());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("OH NO!" + ex);
            }
        }

        ICommand groceriesDeleteComand;
        public ICommand GroceriesDeleteComand =>
            groceriesDeleteComand ?? (groceriesDeleteComand = new Command(async () => await ExecutegroceriesDeleteComandAsync()));

        async Task ExecutegroceriesDeleteComandAsync() {
            try
            {
                await AzureContext.Initialize();
                for (int i = 0; i < _groceryList.Count; i++) {
                    if (groceryList[i].Marc) {
                        AzureContext.deleteGrocery(_groceryList[i]);
                    }
                }

                groceryList.Clear();
                groceryList = new ObservableCollection<Domain.Grocery>(await AzureContext.GetGrocery());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("OH NO!" + ex);
            }

        }



        
    }
}
