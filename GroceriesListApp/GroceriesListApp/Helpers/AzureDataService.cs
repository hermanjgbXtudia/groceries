﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using System.Collections;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.Diagnostics;

namespace GroceriesListApp.Helpers
{
    public class AzureDataService
    {
        public MobileServiceClient MobileService { get; set; }

        IMobileServiceSyncTable<Domain.Grocery> GroceryContext;

        bool isInitialized;

        public async Task Initialize()
        {
            MobileService = new MobileServiceClient("http://grocerystoreservice.azurewebsites.net");

        
            const string path = "syncstore.db";
            //setup our local sqlite store and intialize our table
            var store = new MobileServiceSQLiteStore(path);
            store.DefineTable<Domain.Grocery>();
            await MobileService.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());
     
            //Get our sync table that will call out to azure
            GroceryContext = MobileService.GetSyncTable<Domain.Grocery>();
            isInitialized = true;
        
        }

        public async Task<IEnumerable<Domain.Grocery>> GetGrocery()
        {
            await Initialize();
            await SyncGrocery();
            return await GroceryContext.ToCollectionAsync<Domain.Grocery>();
        }

        public async Task<Domain.Grocery> AddGrocery(Domain.Grocery newitem)
        {
            await Initialize();
            await SyncGrocery();
            await GroceryContext.InsertAsync(newitem);
            return newitem;

        }

        public async void  deleteGrocery(Domain.Grocery item) {
            await Initialize();
            await SyncGrocery();
            await GroceryContext.DeleteAsync(item);
            
        }

        public async Task SyncGrocery()
        {
            try
            {
                await GroceryContext.PullAsync(null, GroceryContext.CreateQuery());
                await MobileService.SyncContext.PushAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to sync Groceries, that is alright as we have offline capabilities: " + ex);
            }
        }

    }
}
